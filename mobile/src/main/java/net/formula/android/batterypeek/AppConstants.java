package net.formula.android.batterypeek;

/**
 * Created by f97one on 14/09/13.
 */
public class AppConstants {
    public static final String PREFERENCE_ENABLE_ON_BOOT = "FlagEnableOnBoot";
    public static final String PREFERENCE_FUZZY_LEVEL = "FlagFuzzyLevel";
    public static final String PREFERENCE_CHARGE_STATE = "FlagChargeState";
    public static final String PREFERENCE_REMAIN_BATTERY_LEVEL = "FlagRemainBatteryLevel";
    public static final String PREFERENCE_BATTERY_TEMPERATURE = "FlagBatteryTemperature";
    public static final String PREFERENCE_TEMPERATURE_UNIT_IS_CELSIUS = "FlagIsCelsius";

    // ファジー度
    /**
     * ファジー度：正確（ファジー度なし）
     */
    public static final int PREF_FUZZY_LEVEL_EXACT = 0;
    /**
     * ファジー度；少しファジー
     */
    public static final int PREF_FUZZY_LEVEL_LITTLE = 1;
    /**
     * ファジー度：中間程度
     */
    public static final int PREF_FUZZY_LEVEL_MEDIUM = 2;
    /**
     * ファジー度：結構ファジー
     */
    public static final int PREF_FUZZY_LEVEL_HARD = 3;
    /**
     * ファジー度：とってもファジー
     */
    public static final int PREF_FUZZY_LEVEL_PRETTY = 4;

}
