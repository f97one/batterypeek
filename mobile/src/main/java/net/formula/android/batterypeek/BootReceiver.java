package net.formula.android.batterypeek;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean enabled = preferences.getBoolean(AppConstants.PREFERENCE_ENABLE_ON_BOOT, false);

        if (enabled) {
            Intent svc = new Intent(context, PowerPeekService.class);
            context.startService(svc);
        }
    }
}
