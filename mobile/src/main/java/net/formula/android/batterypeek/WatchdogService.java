package net.formula.android.batterypeek;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class WatchdogService extends Service {

    public static final String ACCEPT_UNBIND_MESSAGE = WatchdogService.class.getName() + ".ACCEPT_UNBIND_MESSAGE";

    private final IBinder mBinder = new WatchdogServiceBinder();

    private boolean mPairBound;

    public WatchdogService() {
    }

    public class WatchdogServiceBinder extends Binder {
        public WatchdogService getService() {
            return WatchdogService.this;
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            String tag = WatchdogService.class.getSimpleName() + "#onServiceConnected";
            Log.d(tag, name.getClassName() + " からバインドされた");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            startAndBindPair();
        }
    };

    private void startAndBindPair() {
        Intent intent = new Intent(this, PowerPeekService.class);
        if (SvcUtil.isServiceRunning(this, PowerPeekService.class.getCanonicalName())) {
            stopService(intent);
        }
        startService(intent);
        mPairBound = getApplicationContext().bindService(intent, mConnection, BIND_AUTO_CREATE);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // アンバインド受付のBroadcastReceiverを登録
        IntentFilter filter = new IntentFilter(ACCEPT_UNBIND_MESSAGE);
        getApplicationContext().registerReceiver(mUnbindReceiver, filter);

        Intent intent1 = new Intent(this, PowerPeekService.class);
        getApplicationContext().bindService(intent1, mConnection, BIND_AUTO_CREATE);

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mPairBound) {
            getApplicationContext().unbindService(mConnection);
            mPairBound = false;
        }

        // アンバインド受付のBroadcastReceiver解除
        getApplicationContext().unregisterReceiver(mUnbindReceiver);
    }

    /**
     * アンバインドを受け付けるBroadcastReceiver。
     */
    private BroadcastReceiver mUnbindReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mPairBound) {
                getApplicationContext().unbindService(mConnection);
                mPairBound = false;
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            stopSelf();
        }
    };
}
