package net.formula.android.batterypeek;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.preference.PreferenceManager;

import java.math.BigDecimal;

/**
 * ステータス計算を行うユーティリティクラス。<br />
 *<br />
 * Created by HAJIME on 2014/10/05.
 */
public class StatusCalc extends ContextWrapper {

    private Context mContext;
    private SharedPreferences preferences;
    private int fuzzyLevel;

    /**
     * コンストラクタ。
     *
     * @param context Preference取得に使うContext
     */
    public StatusCalc(Context context) {
        super(context);

        this.mContext = context;

        // Preferenceからファジー度設定を取得
        // ファジー度のデフォルト値は「正確」
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        fuzzyLevel = preferences.getInt(
                AppConstants.PREFERENCE_FUZZY_LEVEL, AppConstants.PREF_FUZZY_LEVEL_EXACT);
    }

    /**
     * 保存されている充電状態を、ファジー度に応じた値に加工して返す。
     *
     * @param fuzzyLevel 指定するファジー度
     * @return 充電状態
     */
    public String getChargeState(int fuzzyLevel) {
        // 充電状態（初期値は「不明」にする）
        int state = preferences.getInt(
                AppConstants.PREFERENCE_CHARGE_STATE, BatteryManager.BATTERY_STATUS_UNKNOWN);

        switch (fuzzyLevel) {
            case AppConstants.PREF_FUZZY_LEVEL_EXACT:
                // 「正確」の場合はそのままの値を使うため、何もしない
                break;
            case AppConstants.PREF_FUZZY_LEVEL_LITTLE:
                // 「少し」の場合は、DISCHARGINGをNOT_CHARGINGに変える
                if (state == BatteryManager.BATTERY_STATUS_DISCHARGING) {
                    state = BatteryManager.BATTERY_STATUS_NOT_CHARGING;
                }
                break;
            case AppConstants.PREF_FUZZY_LEVEL_MEDIUM:
                // 「普通」の場合は、上記に加えFULLをNOT_CHARGINGに変える
                if (state == BatteryManager.BATTERY_STATUS_DISCHARGING
                        || state == BatteryManager.BATTERY_STATUS_FULL) {
                    state = BatteryManager.BATTERY_STATUS_NOT_CHARGING;
                }
                break;
            case AppConstants.PREF_FUZZY_LEVEL_HARD:
            case AppConstants.PREF_FUZZY_LEVEL_PRETTY:
                // 「結構」「とっても」は一緒の処理とし、FULL以外はすべてUNKNOWNとする
                if (state != BatteryManager.BATTERY_STATUS_FULL) {
                    state = BatteryManager.BATTERY_STATUS_UNKNOWN;
                }

                break;
        }

        return getExactChargeState(state);
    }

    /**
     * 保存されている充電状態を、ファジー度に応じた値に加工して返す。
     *
     * @return 充電状態
     */
    public String getChargeState() {
        return getChargeState(this.fuzzyLevel);
    }

    /**
     * BatteryManagerで規定されている充電状態に対応する文字列を返す。
     *
     * @param chargeStateFlag 充電状態フラグ
     * @return 充電状態に対応する文字列
     */
    private String getExactChargeState(int chargeStateFlag) {
        String chargeState = "";
        switch (chargeStateFlag) {
            case BatteryManager.BATTERY_STATUS_UNKNOWN:
                chargeState = mContext.getString(R.string.chargeState_Unknown);
                break;
            case BatteryManager.BATTERY_STATUS_CHARGING:
                chargeState = mContext.getString(R.string.chargeState_Charging);
                break;
            case BatteryManager.BATTERY_STATUS_DISCHARGING:
                chargeState = mContext.getString(R.string.chargeState_Discharging);
                break;
            case BatteryManager.BATTERY_STATUS_FULL:
                chargeState = mContext.getString(R.string.chargeState_Full);
                break;
            case BatteryManager.BATTERY_STATUS_NOT_CHARGING:
                chargeState = mContext.getString(R.string.chargeState_NotCharging);
                break;
        }

        return chargeState;
    }

    /**
     * 保存されている残存バッテリーレベルを、ファジー度に応じた値に加工して返す。
     *
     * @param fuzzyLevel 指定するファジー度
     * @return 残存バッテリーレベル
     */
    public String getCurrentRemainLevel(int fuzzyLevel) {
        // 残存バッテリーレベル（初期値は100とする）
        int remainLevel = preferences.getInt(AppConstants.PREFERENCE_REMAIN_BATTERY_LEVEL, 100);
        String percent = " %";

        String ret = "";
        switch(fuzzyLevel) {
            case AppConstants.PREF_FUZZY_LEVEL_EXACT:
                // 「正確」の場合は、保存されている値をそのままパーセント表記にする
                ret = String.valueOf(remainLevel) + percent;
                break;
            case AppConstants.PREF_FUZZY_LEVEL_LITTLE:
                // 「少し」の場合は10%刻み（切り上げ）とする
                ret = mContext.getString(R.string.about) + " " + String.valueOf(Math.ceil(remainLevel / 10) * 10) + percent;
                break;
            case AppConstants.PREF_FUZZY_LEVEL_MEDIUM:
                // 「普通」の場合は1/4単位で表示する。
                if (remainLevel <= 5) {
                    ret = mContext.getString(R.string.almost_empty);
                } else if (remainLevel <= 30) {
                    ret = mContext.getString(R.string.about) + " " + mContext.getString(R.string.quarter);
                } else if (remainLevel <= 65) {
                    ret = mContext.getString(R.string.about) + " " + mContext.getString(R.string.half);
                } else if (remainLevel <= 90) {
                    ret = mContext.getString(R.string.about) + " " + mContext.getString(R.string.three_quarter);
                } else {
                    ret = mContext.getString(R.string.almost_full);
                }
                break;
            case AppConstants.PREF_FUZZY_LEVEL_HARD:
                // 「結構」の場合は、3段階評価とする
                if(remainLevel <= 15) {
                    ret = mContext.getString(R.string.almost_empty);
                } else if (remainLevel <= 85) {
                    ret = mContext.getString(R.string.enough);
                } else {
                    ret = mContext.getString(R.string.almost_full);
                }
                break;
            case AppConstants.PREF_FUZZY_LEVEL_PRETTY:
                // 「とっても」の場合は、「ある」か「ない」かで評価する
                if (remainLevel <= 30) {
                    ret = mContext.getString(R.string.not_enough);
                } else {
                    ret = mContext.getString(R.string.enough);
                }
                break;
        }

        return ret;
    }

        /**
         * 保存されている残存バッテリーレベルを、ファジー度に応じた値に加工して返す。
         *
         * @return 残存バッテリーレベル
         */
    public String getCurrentRemainLevel() {
        return getCurrentRemainLevel(this.fuzzyLevel);
    }

    /**
     * バッテリー温度を華氏に換算する。
     *
     * @param batteryTemp BatteryManagerで取得できるバッテリー温度
     * @return 華氏のバッテリー温度
     */
    public float toFahrenheitDegree(int batteryTemp) {
        // バッテリー温度は0.1度刻みのため、1/10してから華氏に換算する
        float f = (9.0f / 5.0f) * ((float) batteryTemp / 10.0f) + 32.0f;

        return roundInOneDecimal(f);
    }

    private float roundInOneDecimal(float floatValue) {
        BigDecimal decimal = new BigDecimal(String.valueOf(floatValue)).setScale(1, BigDecimal.ROUND_HALF_UP);

        return decimal.floatValue();
    }

    public float toCelsiusDegree(int batteryTemp) {
        float c = ((float) batteryTemp / 10.0f);

        return roundInOneDecimal(c);
    }

    public String getCurrentTemperature(int fuzzyLevel, boolean isCelsius) {
        String ret = "";
        int temp = preferences.getInt(AppConstants.PREFERENCE_BATTERY_TEMPERATURE, 0);

        String unit = "";
        float tempValue = 0;
        if (isCelsius) {
            tempValue = toCelsiusDegree(temp);
            unit = getString(R.string.unit_celsius);
        } else {
            tempValue = toFahrenheitDegree(temp);
            unit = getString(R.string.unit_fahrenheit);
        }

        switch (fuzzyLevel) {
            case AppConstants.PREF_FUZZY_LEVEL_EXACT:
                // 「正確』の場合は、そのまま取得した値を返す
                ret = String.valueOf(tempValue) + unit;
                break;
            case AppConstants.PREF_FUZZY_LEVEL_LITTLE:
                // 「少し』の場合は10度刻みで表示
                ret = getString(R.string.about) + " " + String.valueOf((int) (Math.ceil(tempValue / 10)) * 10) + unit;
                break;
            case AppConstants.PREF_FUZZY_LEVEL_MEDIUM:
                // 「普通」の場合は25度刻み
                ret = getString(R.string.about) + " " + String.valueOf((int) (Math.ceil(tempValue / 25)) * 25) + unit;
                break;
            case AppConstants.PREF_FUZZY_LEVEL_HARD:
            case AppConstants.PREF_FUZZY_LEVEL_PRETTY:
                // 「結構」と「とっても」の場合は、「熱い」か「熱くない」の２択
                if (temp >= 350) {
                    ret = getString(R.string.hot);
                } else {
                    ret = getString(R.string.not_hot);
                }
                break;
        }

        return ret;
    }

    public String getCurrentTemperature(int fuzzyLevel) {
        boolean isCelsius = preferences.getBoolean(AppConstants.PREFERENCE_TEMPERATURE_UNIT_IS_CELSIUS, true);
        return getCurrentTemperature(fuzzyLevel, isCelsius);
    }

    public String getCurrentTemperature() {
        return getCurrentTemperature(this.fuzzyLevel);
    }

}
