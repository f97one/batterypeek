package net.formula.android.batterypeek;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

public class PowerPeekService extends Service {

    private PowerReceiver powerReceiver;
    private final IBinder mBinder = new PowerPeekServiceBinder();
    private boolean mPairBound;

    public class PowerPeekServiceBinder extends Binder {
        public PowerPeekService getService() {
            return PowerPeekService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            String tag = PowerPeekService.class.getSimpleName() + "#onServiceConnected";
            Log.d(tag, name.getClassName() + " からバインドされた");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            startAndBindPair();
        }
    };

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        // PowerReceiverの開始
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        powerReceiver = new PowerReceiver();
        getApplicationContext().registerReceiver(powerReceiver, intentFilter);

        // WatchdogServiceの開始
        if (!SvcUtil.isServiceRunning(this, WatchdogService.class.getCanonicalName())) {
            startWatchdogService();
        }
        mPairBound = bindWatchdogService();

        return START_REDELIVER_INTENT;
    }

    private void startAndBindPair() {
        Intent intent = new Intent(this, WatchdogService.class);
        if (mPairBound) {
            getApplicationContext().unbindService(mConnection);
            mPairBound = false;
        }
        stopService(intent);
        startService(intent);
        mPairBound = getApplicationContext().bindService(intent, mConnection, BIND_AUTO_CREATE);
    }

    private void startWatchdogService() {
        Intent i = new Intent(this, WatchdogService.class);

        if (SvcUtil.isServiceRunning(this, WatchdogService.class.getCanonicalName())) {
            stopService(i);
        }

        startService(i);
    }

    private boolean bindWatchdogService() {
        Intent i = new Intent(this, WatchdogService.class);
        return getApplicationContext().bindService(i, mConnection, BIND_AUTO_CREATE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Intent i = new Intent(WatchdogService.ACCEPT_UNBIND_MESSAGE);
        sendBroadcast(i);

        NotificationManagerCompat compat = NotificationManagerCompat.from(this);
        compat.cancel(PowerReceiver.NOTIFICATION_ID);

        getApplicationContext().unregisterReceiver(powerReceiver);

        getApplicationContext().unbindService(mConnection);
        mPairBound = false;
    }

}
