package net.formula.android.batterypeek;

import android.app.ActivityManager;
import android.content.Context;

import java.util.List;

/**
 * Created by HAJIME on 15/03/08.
 */
public class SvcUtil {

    private SvcUtil() { }

    public static boolean isServiceRunning(Context context, String targetServiceCanonicalName) {
        boolean ret = false;

        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> infoList = manager.getRunningServices(Integer.MAX_VALUE);

        for (ActivityManager.RunningServiceInfo info : infoList) {
            if (targetServiceCanonicalName.equals(info.service.getClassName())) {
                ret = true;
                break;
            }
        }

        return ret;
    }
}
