package net.formula.android.batterypeek;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class NotificationUpdaterService extends IntentService {
    /**
     * Notificationを更新するときに指定するAction
     */
    public static final String ACTION_UPDATE_NOTIFICATION = "net.formula.android.batterypeek.action.UPDATE_NOTIFICATION";
    /**
     * Notificationをキャンセルするときに指定するAction
     */
    public static final String ACTION_CANCEL_NOTIFICATION = "net.formula.android.batterypeek.action.CANCEL_NOTIFICATION";

    public static final int NOTIFICATION_ID = 0x7fff8001;

    /**
     * 充電状態を引き渡す時のExtra
     */
    private static final String EXTRA_CHARGE_STATE = "net.formula.android.batterypeek.extra.CHARGE_STATE";
    /**
     * 現残量を引き渡す時のExtra
     */
    private static final String EXTRA_CURRENT_REMAIN = "net.formula.android.batterypeek.extra.CURRENT_REMAIN";

    public NotificationUpdaterService() {
        super("NotificationUpdaterService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startCancelNotification(Context context) {
        Intent intent = new Intent(context, NotificationUpdaterService.class);
        intent.setAction(ACTION_CANCEL_NOTIFICATION);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startUpdateNotification(Context context, String chargeState, String currentRemain) {
        Intent intent = new Intent(context, NotificationUpdaterService.class);
        intent.setAction(ACTION_UPDATE_NOTIFICATION);
        intent.putExtra(EXTRA_CHARGE_STATE, chargeState);
        intent.putExtra(EXTRA_CURRENT_REMAIN, currentRemain);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_UPDATE_NOTIFICATION.equals(action)) {
                final String chargeState = intent.getStringExtra(EXTRA_CHARGE_STATE);
                final String currentRemain = intent.getStringExtra(EXTRA_CURRENT_REMAIN);
                handleUpdateNotification(chargeState, currentRemain);
            } else if (ACTION_CANCEL_NOTIFICATION.equals(action)) {
                handleCancelNotification();
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleUpdateNotification(String chargeState, String currentRemain) {
        // NotificationCompatでWearableにもNotificationを投げる
        Intent v = new Intent(getApplicationContext(), MainActivity.class);
        v.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        v.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, v, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.ic_stat)
                .setContentText(getString(R.string.chargeState) + " " + chargeState)
                .setContentTitle(getString(R.string.remain) + " " + currentRemain)
                .setContentIntent(pi);
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(getApplicationContext());
        notificationManagerCompat.notify(NOTIFICATION_ID, builder.build());
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleCancelNotification() {
        NotificationManagerCompat nmc = NotificationManagerCompat.from(getApplicationContext());
        nmc.cancel(NOTIFICATION_ID);
    }
}
