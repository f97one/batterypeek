package net.formula.android.batterypeek;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MsgDialogFragment#getInstance} factory method to
 * create an instance of this fragment.
 */
public class MsgDialogFragment extends DialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    public static final String ARG_STACK_TRACE_STRINGS = "ArgStackTraceStrings";

    public static final String FRAGMENT_TAG = MsgDialogFragment.class.getName() + ".FRAGMENT_TAG";

    // TODO: Rename and change types of parameters
    private String mParam1;

    public MsgDialogFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment MsgDialogFragment.
     */
    public static MsgDialogFragment getInstance(String param1) {
        MsgDialogFragment fragment = new MsgDialogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_STACK_TRACE_STRINGS, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_STACK_TRACE_STRINGS);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(mParam1).setPositiveButton(android.R.string.ok, null);

        return builder.create();
    }
}
