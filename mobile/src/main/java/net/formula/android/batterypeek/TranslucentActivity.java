package net.formula.android.batterypeek;

import android.app.Activity;
import android.os.Bundle;

public class TranslucentActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String errMeg = getIntent().getStringExtra(MsgDialogFragment.ARG_STACK_TRACE_STRINGS);

        MsgDialogFragment fragment = MsgDialogFragment.getInstance(errMeg);
        fragment.show(getFragmentManager(), MsgDialogFragment.FRAGMENT_TAG);
    }
}
