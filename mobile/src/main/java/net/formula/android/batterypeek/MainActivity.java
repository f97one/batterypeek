package net.formula.android.batterypeek;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.List;


public class MainActivity extends Activity {

    private TextView tvCurrentChargeState;
    private TextView tvCurrentRemainLevel;
    private TextView tvCurrentTemperatureValue;
    private CheckBox checkBoxStartOnBoot;
    private TextView textViewDescEnableOnBoot;
    private Spinner spinner;
    private TextView textViewDescFuzzyLevel;
    private RadioButton radioButtonCelsius;
    private RadioButton radioButtonFahrenheit;
    private RadioGroup radioGroupTempUnit;
    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // ViewId取得
        tvCurrentChargeState = (TextView) findViewById(R.id.tvCurrentChargeState);
        tvCurrentRemainLevel = (TextView) findViewById(R.id.tvCurrentRemainLevel);
        tvCurrentTemperatureValue = (TextView) findViewById(R.id.tvCurrentTemperatureValue);
        checkBoxStartOnBoot = (CheckBox) findViewById(R.id.checkBoxStartOnBoot);
        textViewDescEnableOnBoot = (TextView) findViewById(R.id.textViewDescEnableOnBoot);
        spinner = (Spinner) findViewById(R.id.spinner);
        textViewDescFuzzyLevel = (TextView) findViewById(R.id.textViewDescFuzzyLevel);
        radioButtonCelsius = (RadioButton) findViewById(R.id.radioButtonCelsius);
        radioButtonFahrenheit = (RadioButton) findViewById(R.id.radioButtonFahrenheit);
        radioGroupTempUnit = (RadioGroup) findViewById(R.id.radioGroupTempUnit);

        // リスナーのセット
        checkBoxStartOnBoot.setOnCheckedChangeListener(new CheckBoxCallback());
        radioGroupTempUnit.setOnCheckedChangeListener(new RadioGroupCallback());
        spinner.setOnItemSelectedListener(new SpinnerCallback());

        // Spinnerへのアイテムセット
        String[] fuzzyArray = getResources().getStringArray(R.array.FuzzyLevelArray);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, fuzzyArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();

        restartService();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        int fuzzyLevel =  preferences.getInt(AppConstants.PREFERENCE_FUZZY_LEVEL, 0);
        // Preferenceの復元
        spinner.setSelection(fuzzyLevel, false);
        checkBoxStartOnBoot.setChecked(
                preferences.getBoolean(AppConstants.PREFERENCE_ENABLE_ON_BOOT, false));
        boolean isCelsius = preferences.getBoolean(
                AppConstants.PREFERENCE_TEMPERATURE_UNIT_IS_CELSIUS, true);
        if (isCelsius) {
            radioButtonCelsius.setChecked(true);
        } else {
            radioButtonFahrenheit.setChecked(true);
        }

        StatusCalc calc = new StatusCalc(this);
        setStatus(calc, fuzzyLevel);

        adView.resume();
    }

    private void setStatus(StatusCalc calc, int fuzzyLevel) {
        boolean isCelsius = radioButtonCelsius.isChecked();

        // 充電状態
        tvCurrentChargeState.setText(calc.getChargeState(fuzzyLevel));
        // 残存バッテリーレベル
        tvCurrentRemainLevel.setText(calc.getCurrentRemainLevel(fuzzyLevel));
        // 温度
        tvCurrentTemperatureValue.setText(calc.getCurrentTemperature(fuzzyLevel, isCelsius));
    }

    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putInt(AppConstants.PREFERENCE_FUZZY_LEVEL, spinner.getSelectedItemPosition());
        editor.putBoolean(AppConstants.PREFERENCE_ENABLE_ON_BOOT, checkBoxStartOnBoot.isChecked());
        boolean checked = false;
        if (radioButtonCelsius.isChecked()) {
            checked = true;
        } else if (radioButtonFahrenheit.isChecked()) {
            checked = false;
        }
        editor.putBoolean(AppConstants.PREFERENCE_TEMPERATURE_UNIT_IS_CELSIUS, checked);
        editor.apply();

        adView.pause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_stop_service) {
            Intent broadcast = new Intent(WatchdogService.ACCEPT_UNBIND_MESSAGE);
            sendBroadcast(broadcast);

            Intent i = new Intent(this, PowerPeekService.class);
            stopService(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void restartService() {
        Intent i = new Intent(this, PowerPeekService.class);

        if (isServiceRunning(PowerPeekService.class.getCanonicalName())) {
            if (stopService(i)) {
                Log.d("restartService", "PowerPeekService has stopped.");
            } else {
                Log.d("restartService", "PowerPeekService is already stopped.");
            }
        }
        ComponentName cName = startService(i);
        if (cName == null || !cName.getClassName().equals(PowerPeekService.class.getName())) {
            Log.w("restartService", "PowerPeekService could not start.");
        } else if (cName.getClassName().equals(PowerPeekService.class.getName())) {
            Log.d("restartService", "PowerPeekService was started successfully.");
        }

    }

    @Override
    protected void onDestroy() {
        adView.destroy();

        super.onDestroy();
    }

    /**
     * サービスが稼働しているかを確認する
     * @param svcName 確認対象のサービスの正規名
     * @return 対象サービスが稼働している場合はtrue、停止中ならfalse
     */
    private boolean isServiceRunning(String svcName){
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> infos = manager.getRunningServices(Integer.MAX_VALUE);

        for (ActivityManager.RunningServiceInfo info : infos) {
            if(svcName.equals(info.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private class CheckBoxCallback implements CompoundButton.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                textViewDescEnableOnBoot.setText(R.string.desc_on);
            } else {
                textViewDescEnableOnBoot.setText(R.string.desc_off);
            }
        }
    }

    private class RadioGroupCallback implements RadioGroup.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            StatusCalc calc = new StatusCalc(MainActivity.this);
            int fuzzyLevel = spinner.getSelectedItemPosition();

            boolean isCelsius = checkedId == radioButtonCelsius.getId();

            tvCurrentTemperatureValue.setText(calc.getCurrentTemperature(fuzzyLevel, isCelsius));
        }
    }

    private class SpinnerCallback implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String[] fuzzyDesc = getResources().getStringArray(R.array.FuzzyLevelDetailArray);
            textViewDescFuzzyLevel.setText(fuzzyDesc[position]);

            StatusCalc calc = new StatusCalc(MainActivity.this);
            setStatus(calc, position);

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }
}
