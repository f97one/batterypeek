package net.formula.android.batterypeek;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;

public class PowerReceiver extends BroadcastReceiver {

    public static final int NOTIFICATION_ID = 0x7fff9001;

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(Intent.ACTION_BATTERY_CHANGED)) {

            Log.d("PowerReceiver#onReceive", "Received battery changed event.");

            // 充電状態、残存レベル、温度をそれぞれ取得
            int currentStatus = intent.getIntExtra(BatteryManager.EXTRA_STATUS, 0);
            int remainBatteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            int currentTemp = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0);

            // 充電状態
            StatusCalc calc = new StatusCalc(context);

            try {
                String chargeState = calc.getChargeState();
                String currentRemain = calc.getCurrentRemainLevel();
//            String temp = calc.

                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
                SharedPreferences.Editor editor = preferences.edit();

                editor.putInt(AppConstants.PREFERENCE_CHARGE_STATE, currentStatus);
                editor.putInt(AppConstants.PREFERENCE_REMAIN_BATTERY_LEVEL, remainBatteryLevel);
                editor.putInt(AppConstants.PREFERENCE_BATTERY_TEMPERATURE, currentTemp);
                editor.apply();

                // NotificationCompatでWearableにもNotificationを投げる
                Intent v = new Intent(context, MainActivity.class);
                v.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                v.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                PendingIntent pi = PendingIntent.getActivity(context, 0, v, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_stat)
                        .setContentText(context.getString(R.string.chargeState) + " " + chargeState)
                        .setContentTitle(context.getString(R.string.remain) + " " + currentRemain)
                        .setContentIntent(pi);
//                        .setOngoing(true);
                NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
                // 一旦キャンセルする
                notificationManagerCompat.cancel(NOTIFICATION_ID);
                notificationManagerCompat.notify(NOTIFICATION_ID, builder.build());
            } catch (Exception e) {
                e.printStackTrace();

                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                pw.flush();
                String str = sw.toString();

                Intent intent1 = new Intent(context, TranslucentActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent1.putExtra(MsgDialogFragment.ARG_STACK_TRACE_STRINGS, str);
                context.startActivity(intent1);
            }
        }
    }
}
